import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { PalesterModel } from './models/palester.model';


@Injectable ({
    providedIn: 'root'
  })
  
  export class PalesterService {
  constructor(private http: HttpClient) { }
  
  getAllPalester(): Observable<PalesterModel>{
      return this.http.get('assets/palester.json') as Observable<PalesterModel>;
  }
    createPalester() {
  } 
  
  deletePalester(id) {

    
  }
  
  updatePalester(id) {
    
  }
  
  getPalesterById(id) {
  
  }
  }
  