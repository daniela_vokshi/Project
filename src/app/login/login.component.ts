import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { UserModel } from '../models/user.model';
import { UserService } from '../user.service';
import { FormsModule } from '@angular/forms';




@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  allusers: UserModel;
  isCorrect: boolean = null;
  firebaseusers;

  constructor( private route: Router,
    private userService: UserService,
    private db: AngularFirestore) { }

  ngOnInit() {
    this.userService.getAllusers().subscribe(users => {
      this.allusers = users;
    })

    this.db.collection('users').valueChanges().subscribe((users) => {
      this.firebaseusers = users;
      console.log(users);
    })

  }
  signin(email: string, password: string) {
    this.firebaseusers.forEach( (user: UserModel) => {
    if(email === user.email && password === user.password) {
      this.route.navigate(['palester'])
    }else {
      this.isCorrect = false;
    }
    })
  }

}
