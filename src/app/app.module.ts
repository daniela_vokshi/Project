import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomeComponent } from './home/home.component';
import {environment} from '../environments/environment'
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from '@angular/common/http';
import { RegisterComponent } from './register/register.component';
import { ReactiveFormsModule } from '@angular/forms';
import { PalesterComponent } from './palester/palester.component';
import { AddusersComponent } from './addusers/addusers.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HomeNavBarComponent } from './home-nav-bar/home-nav-bar.component';
import { PriceComponent } from './price/price.component';
import { HelpComponent } from './help/help.component';




const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'palester', component: PalesterComponent},
  {path: 'addusers', component: AddusersComponent},
  {path: 'price', component: PriceComponent},
  {path:'help', component: HelpComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavBarComponent,
    HomeComponent,
    RegisterComponent,
    PalesterComponent,
    AddusersComponent,
    HomeNavBarComponent,
    PriceComponent,
    HelpComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule
    
    
   

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
