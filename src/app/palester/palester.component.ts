import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { PalesterModel } from '../models/palester.model';
import { PalesterService } from '../palester.service';

@Component({
  selector: 'app-palester',
  templateUrl: './palester.component.html',
  styleUrls: ['./palester.component.css']
})
export class PalesterComponent implements OnInit {
 Palester: PalesterModel[];
 firebasePalester;
  constructor(private db: AngularFirestore,
    privatepalesterService: PalesterService) { }

  ngOnInit(): void {
    this.db.collection('Palester').valueChanges().subscribe((Palester: PalesterModel[]) => {
      this.Palester = Palester;
      console.log(this.Palester)
    })

  }   
}
