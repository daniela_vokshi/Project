import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PalesterComponent } from './palester.component';

describe('PalesterComponent', () => {
  let component: PalesterComponent;
  let fixture: ComponentFixture<PalesterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PalesterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PalesterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
