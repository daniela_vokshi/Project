import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-addusers',
  templateUrl: './addusers.component.html',
  styleUrls: ['./addusers.component.css']
})
export class AddusersComponent implements OnInit {
addForm = this.fb.group({
  Name: [''],
  Last_Name: [''],
  email: [''],
  Birthdate: [''],
  Phone: [''],
  Gender: [''],
  Address: [''],
  Duration: [''],
  Time: ['']
})
  constructor(private route: Router, private fb: FormBuilder, private db: AngularFirestore) { }

  ngOnInit(): void {
  }

  adduser(Palester) {
    console.log(Palester)
    this.db.collection('Palester').add(Palester)

    this.route.navigate(['palester'])
  }

}
