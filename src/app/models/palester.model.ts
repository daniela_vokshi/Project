export interface PalesterModel  {
    Name: string;
    Last_Name: string;
    email: string;
    Birthdate: Date;
    Phone: string;
    Address: string;
    Gender: string;
    Time: string;
    Duration: string;
}