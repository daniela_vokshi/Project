export interface UserModel {
    name: string;
    email: string;
    password: string;
    birthdate: Date;
    mobile: string;
    address: string;
    gender: string;
}
