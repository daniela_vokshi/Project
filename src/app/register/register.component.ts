import { Component, OnInit } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  registerForm = this.fb.group({
    name: [''],
    email: [''],
    password: [''],
    birthdate: [''],
    mobile: [''],
    gender: [''],
    address:['']
  })


  constructor(private route: Router, private fb: FormBuilder, private db: AngularFirestore) { }

  ngOnInit(): void {
  }
  
  createUser(user) {
    console.log(user)
    this.db.collection('users').add(user);

    this.route.navigate(['login'])


  }


}
