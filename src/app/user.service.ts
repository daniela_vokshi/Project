import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http'
import { UserModel } from './models/user.model';

@Injectable ({
  providedIn: 'root'
})

export class UserService {
constructor(private http: HttpClient) { }

getAllusers(): Observable<UserModel>{
    return this.http.get('assets/users.json') as Observable<UserModel>;
}
  createuser() {
} 

deleteuser(id) {
  // logjika per fshirjen e nje perdoruesi nga sistemi
}

updateuser(id) {
  // logjika per perditesimin e te dhenave  te perdoruesit ne sistem
}

getuserById(id) {

}
}
